/**
 *
 */
package sample01;

/**
 * @author Y.Matsumoto
 * タイムゾーン情報のない日時を扱うクラスのサンプル
 *
 */
public class LocalDateTimeSample01 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		//現在日時をカプセル化したLocalDateTime型のインスタンスを取得し d に代入
		java.time.LocalDateTime d = java.time.LocalDateTime.now();

		//dより日付時刻の各要素を取得
		int year = d.getYear();
		System.out.println(year);

		int month = d.getMonthValue();
		System.out.println(month);

		int day_of_month = d.getDayOfMonth();
		System.out.println(day_of_month);

		int hour = d.getHour();
		System.out.println(hour);

		int min = d.getMinute();
		System.out.println(min);

		int sec = d.getSecond();
		System.out.println(sec);

		int nano = d.getNano();
		System.out.println(nano);

	}

}
