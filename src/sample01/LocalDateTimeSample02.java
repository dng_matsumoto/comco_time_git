package sample01;

import java.time.LocalDateTime;

/**
 *
 * @author Y.Matsumoto
 *
 */
public class LocalDateTimeSample02 {

	public static void main(String[] args) {
		//日時を指定してLocalDateTimeのインスタンスを生成するサンプル

		//年月日時分秒を指定（おかしな値は例外になります）
		LocalDateTime d = LocalDateTime.of(2015, 12, 28, 22, 20, 0);

		//dより日付時刻の各要素を取得
		int year = d.getYear();
		System.out.println(year);

		int month = d.getMonthValue();
		System.out.println(month);

		int day_of_month = d.getDayOfMonth();
		System.out.println(day_of_month);

		int hour = d.getHour();
		System.out.println(hour);

		int min = d.getMinute();
		System.out.println(min);

		int sec = d.getSecond();
		System.out.println(sec);

		int nano = d.getNano();
		System.out.println(nano);


	}

}
