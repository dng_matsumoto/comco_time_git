package sample01;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author Y.Matsumoto
 *
 */
public class LocalDateTimeSample06 {

	public static void main(String[] args) {

		//今の日時のインスタンスを生成
		LocalDateTime d0 = LocalDateTime.now();

		//フォーマッタ
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy年MM月dd日 HH時mm分ss秒");

		//書式をつけて文字列化
		String s = d0.format(dtf);

		System.out.println(s);
	}

}
