package sample01;

import java.time.LocalDateTime;

/**
 *
 * @author Y.Matsumoto
 *
 */
public class LocalDateTimeSample03 {

	public static void main(String[] args) {
		//既存のインスタンスの日時を変更して新たなLocalDateTimeのインスタンスを生成するサンプル

		//年月日時分秒を指定してインスタンスを生成(2015-12-15 23:30:59)
		LocalDateTime d0 = LocalDateTime.of(2015, 12, 15, 23, 30, 59);

		//d0の２０日後のインスタンスを生成する => 2016-01-04 23:30:59
		LocalDateTime d = d0.plusDays(20);

		//dより日付時刻の各要素を取得
		int year = d.getYear();
		System.out.println(year);

		int month = d.getMonthValue();
		System.out.println(month);

		int day_of_month = d.getDayOfMonth();
		System.out.println(day_of_month);

		int hour = d.getHour();
		System.out.println(hour);

		int min = d.getMinute();
		System.out.println(min);

		int sec = d.getSecond();
		System.out.println(sec);

		int nano = d.getNano();
		System.out.println(nano);

	}

}
