/**
 * 
 */
package sample02;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * タイムゾーン付き日付時刻APIの解説用
 *
 * @author y.matsumoto
 *
 */
public class ZonedDateTimeSample01 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//現在日時を取得し、zdt変数に代入
		ZonedDateTime zdt = ZonedDateTime.now();

		//zdt変数の内容を表示
		System.out.println(zdt);
		
		//フォーマッタを作成しfに代入
		DateTimeFormatter f = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		
		//フォーマッタfを使って文字列に変換
		//変換結果をformattedに代入
		String formatted = zdt.format(f);
		
		//formattedを表示
		System.out.println(formatted);
	}

}
