/**
 * 
 */
package convert.sql;

import java.time.Instant;

/**
 * java.sql.Dateやjava.sql.Timeやjava.sql.Timestampから LocalDateやLocalTime等に変換する
 * 
 * サンプル
 * 
 * @author Matsumoto
 *
 */
public class ConvertLocalDateLocalTimeToSqlDate {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		long now = Instant.now().toEpochMilli();

		//java.sql.Date から java.time.LocalDate

		//現在日時を格納する　java.sql.Dateを作る
		java.sql.Date date = new java.sql.Date(now);

		//java.time.LocalDateに変換
		java.time.LocalDate ld = date.toLocalDate();
		System.out.println(ld);

		
		
		//---------------------------------------------
		// java.sql.Time から java.time.LocalTime
		
		//現在時刻を格納するjava.sql.Timeを作る
		java.sql.Time time = new java.sql.Time(now);

		//java.time.LocalTimeに変換
		java.time.LocalTime lt = time.toLocalTime();
		System.out.println(lt);

		
		
		
		//---------------------------------------------
		// java.sql.Timestamp から java.time.LocalDateTime
		
		//現在日時を格納するjava.sql.Timestampを作る
		java.sql.Timestamp ts = new java.sql.Timestamp(now);

		//java.time.LocalDateTimeに変換
		java.time.LocalDateTime ldt = ts.toLocalDateTime();
		System.out.println(ldt);

	}

}
