/**
 * 
 */
package convert.sql;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

/**
 * LocalDateとLocalTimeから java.sql.Timestampを得るサンプル
 * 
 * 日本時間を基準にしています
 * 
 * @author Matsumoto
 *
 */
public class ConvertSqlDateToLocalDateLocalTime {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		//java.sql.Timestampに変換


		//  a)現在日付を格納する　LocalDateを作る
		LocalDate nowd = LocalDate.now();

		//  b)現在時刻を格納するLocalTimeを作る
		LocalTime nowt = LocalTime.now();

		//  c)この地域のタイムゾーンを作る
		ZoneId tz = ZoneId.of("Asia/Tokyo");

		//1) aとbを基にLocalDateTimeを作る
		LocalDateTime nowdt = LocalDateTime.of(nowd, nowt);

		//2)１とcを基に次にZonedDateTimeを作る
		ZonedDateTime nowzdt = nowdt.atZone(tz);

		// 3) 2を基にunix時間を求める
		long epoch = nowzdt.toEpochSecond();

		System.out.println(epoch);

		// 4) 3を基にjava.sql.Timestampを作る
		java.sql.Timestamp ts = new Timestamp(epoch);

		System.out.println(ts);

	}

}
